# Set the base image for subsequent instructions
FROM php:7.4

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq git zip unzip curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev \
     && apt-get install -y libzip-dev

RUN pecl install mcrypt-1.0.4
RUN docker-php-ext-enable mcrypt
# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install pdo_mysql zip

# Install Composer
RUN curl --silent --show-error "https://getcomposer.org/installer" | php -- --install-dir=/usr/local/bin --filename=composer

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~2.0"
